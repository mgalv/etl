class UnitsController < ApplicationController
  def index
    @units = Unit.all
  end

  def import
    Etl.config do |c|
      c.model = Unit
      c.folder_path = '/data'
      c.base_path = Rails.root.to_s
    end

    Etl.import
    redirect_to units_path
  end
end
