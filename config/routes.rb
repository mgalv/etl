Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :units, only: [:index] do
    collection do
      get 'import'
    end
  end

  root 'units#index'
end
