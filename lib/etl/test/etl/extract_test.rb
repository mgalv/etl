require "test_helper"
require 'minitest/autorun'

class UserModel
end

describe Etl::Extract, '.execute' do
  let(:model) { UserModel }
  let(:folder_path) { File.dirname(File.expand_path(__FILE__)) + '/../fixtures/data' }
  subject { Etl::Extract }

  it 'expects a model' do
    extract = subject.execute(model: model, folder_path: folder_path)
    assert_equal extract.model, model
  end

  it 'expects a folder_path' do
    extract = subject.execute(model: model, folder_path: folder_path)
    assert_equal extract.folder_path, folder_path
  end

  it 'returns an exception when no model present' do
    assert_raises(LightService::ExpectedKeysNotInContextError) { subject.execute(folder_path: folder_path) }
  end

  it 'returns an exception when no folder_path present' do
    assert_raises(LightService::ExpectedKeysNotInContextError) { subject.execute(model: model) }
  end

  it 'returns data promise when success' do
    extract = subject.execute(model: model, folder_path: folder_path)
    assert_kind_of Array, extract.data
  end
end

