require "test_helper"
require 'minitest/autorun'

class UserModel
  def attributes
    {}
  end

  def self.create(*args)
    raise Error
  end
end

describe Etl::Load, '.execute' do
  let(:model) { UserModel }
  subject { Etl::Load }

  it 'expects a model' do
    extract = subject.execute(model: model, transformed_data: [])
    assert_equal extract.model, model
  end

  it 'expects a transformed_data' do
    extract = subject.execute(model: model, transformed_data: [])
    assert_equal extract.transformed_data, []
  end

  it 'returns an exception when no model present' do
    assert_raises(LightService::ExpectedKeysNotInContextError) { subject.execute(transformed_data: []) }
  end

  it 'returns an exception when no data present' do
    assert_raises(LightService::ExpectedKeysNotInContextError) { subject.execute(model: model) }
  end

  it 'returns an exception when no data present' do
    assert_raises(LightService::ExpectedKeysNotInContextError) { subject.execute(model: model) }
  end

  it 'returns transformed_data promise when success' do
    extract = subject.execute(model: model, transformed_data: [1])
    assert extract.message, 'There was an error when creating a UserModel'
  end
end

