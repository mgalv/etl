require "test_helper"
require 'minitest/autorun'

class UserModel
  def attributes
    {}
  end
end

describe Etl::Transform, '.execute' do
  let(:model) { UserModel }
  subject { Etl::Transform }

  it 'expects a model' do
    extract = subject.execute(model: model, data: [])
    assert_equal extract.model, model
  end

  it 'expects a data' do
    extract = subject.execute(model: model, data: [])
    assert_equal extract.data, []
  end

  it 'returns an exception when no model present' do
    assert_raises(LightService::ExpectedKeysNotInContextError) { subject.execute(data: []) }
  end

  it 'returns an exception when no data present' do
    assert_raises(LightService::ExpectedKeysNotInContextError) { subject.execute(model: model) }
  end

  it 'returns transformed_data promise when success' do
    extract = subject.execute(model: model, data: [])
    assert_kind_of Array, extract.transformed_data
  end
end

