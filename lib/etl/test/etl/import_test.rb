require "test_helper"
require 'minitest/autorun'

class UserModel
  def attributes
    {}
  end

  def self.create(*args)
    raise Error
  end
end

describe Etl::Import, '.call' do
  let(:model) { UserModel }
  let(:extract) { Etl::Transform }
  let(:transform) { Etl::Extract }
  let(:load) { Etl::Load }
  let(:folder_path) { File.dirname(File.expand_path(__FILE__)) + '/../fixtures/data' }
  subject { Etl::Import }

  it 'expects to execute the load' do
    subject.call(model: model, folder_path: folder_path)
    assert_send([load, :execute, model: model, transformed_data: []])
  end
end
