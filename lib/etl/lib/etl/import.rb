module Etl
  class Import
    extend LightService::Organizer

    def self.call(model:, folder_path: )
      with(model: model, folder_path: folder_path).reduce(
          Extract,
          Transform,
          Load
      )
    end
  end
end
