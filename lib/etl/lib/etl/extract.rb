module Etl
  class Extract
    extend LightService::Action
    expects :model, :folder_path
    promises :data

    executed do |context|
      csv = parse_csv(folder_path: context.folder_path)
      json = parse_json(folder_path: context.folder_path)
      context.data = csv + json.flatten
    end

    def self.parse_csv(folder_path:)
      filename =  folder_path + '/csv/units.csv'
      CSV.open(filename, headers: :first_row).map(&:to_h)
    end

    def self.parse_json(folder_path:)
      filename = folder_path + '/json/unit_groups.json'
      raw_text = File.read(filename)
      groups = JSON.parse(raw_text)['unit_groups']
      groups.map do |group|
        id = group['id']
        filename = folder_path + "/json/#{id}_units.json"
        raw_text = File.read(filename)
        JSON.parse(raw_text)['units']
      end.flatten
    end
  end
end
