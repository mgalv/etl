module Etl
  class Load
    extend LightService::Action
    expects :model, :transformed_data

    executed do |context|
      begin
        context.transformed_data.each do |transformed_element|
          context.model.create transformed_element
        end
      rescue => e
        context.fail!("There was an error when creating a #{context.model.name}")
      end
    end
  end
end
