module Etl
  class Transform
    extend LightService::Action
    expects :model, :data
    promises :transformed_data

    executed do |context|
      element = context.model.new
      attributes = parse_attributes(element)
      context.data.size
      context.transformed_data = context.data.map do |data_element|
        transform(attributes: attributes, raw_element: data_element)
      end
    end

    def self.parse_attributes(instance)
      instance.attributes.keys
    end

    def self.transform(attributes: [], raw_element: {})
      params = {}

      attributes.each do |attribute|
        next if attribute == 'id'

        if attribute == 'uom'
          params[attribute] = raw_element['id']
        else
          params[attribute] = raw_element[attribute]
        end
      end

      params.symbolize_keys
    end
  end
end
