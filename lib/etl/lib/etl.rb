require "etl/version"
require 'json'
require "csv"
require 'active_support'
require 'active_model'
require 'light-service'

require 'active_support/core_ext'
require 'active_support/inflector'

require 'etl/import'
require 'etl/extract'
require 'etl/transform'
require 'etl/load'

module Etl
  mattr_accessor :model, :folder_path, :base_path
  mattr_reader :result

  def self.config
    yield self
  end

  def self.import
    @@result = Import.call(model: model, folder_path: base_path + folder_path)
  end
end
